<?php

namespace app\view;
class MainView extends \ruechgrat\mvc\FastView{
    protected $users;
    protected $user;
    public function __construct() {
        parent::__construct;
   
        $this->title = 'Hello Framework';
        $this->cacheDisabled = TRUE;
    }
    
    public function renderFrontPage($users){
        
        $this->users = $users;
        
        $this->pageContent = $this->getCompiledTpl("main/main");
        return $this->renderFrontPage();
    }
    public function renderEditPage(\app\mapper\User $user){
        $this->user = $user;
        $this->pageContent = $this->getCompiledTpl("main/edit");
        return $this->renderFrontPage();
        
    }
            
}
