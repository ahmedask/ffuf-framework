<?php

namespace app\dbo;

/**
 *
 * @author ahmedaljanabi
 */
class User extends \rueckgrat\db\Mapper {
    
    protected $prename;
    protected $name;
    protected $mail;
    public function __construct() {
        parent::__construct;
    }
 public function getPrename() {
     return $this->prename;
    }
 public function getName() {
     return $this->name;
    }
    public function getMail() {
     return $this->mail;
    }
    }
