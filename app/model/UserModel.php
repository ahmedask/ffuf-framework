<?php

namespace app\model;

/**
 * Description of UserModel
 *
 * @author ahmedaljanabi
 */
class UserModel extends \rueckgrat\mvc\defaultDBModel   {

    protected $validator;
    

    public function __construct() {
        parent::__construct("user");
        
        $this->validator = new \ruechgrat\security\InputValidator();
    }
    public function getAllUsers(){
        
        $stmnt =$this->db->query("SELECT * FROM user");
        $users = array();
        
        while ($row = $stmnt->fetch()){
            
              $user = new \app\mapper\User();
              $user->map($row);
              
              $users[] = $user; 
            
        }
        return $users;
    }
    
    public function createUser(\app\mapper\User $user){
        $this->validator->validate(new \app\validator\UserValidator($user));
        $this->create($user);
        
        
    }
    
    public function getById($id){
        $user = new \app\mapper\User();
        
        $row = $this->get($id);
        
        $user->map($row);
        
        return $user;
    }
    public function deleteUser(\app\mapper\User $user){
        $this->delete($user);
        
    }
    public function editUser(\app\mapper\User $user){
                $this->validator->validate(new \app\validator\UserValidator($user));
                $this->save($user);
    }
}
