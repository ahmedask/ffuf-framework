<?php

namespace app\validator;
use rueckgrat\security\ValidationRules;
use rueckgrat\security\ValidationRule;

/**
 * Description of newPHPClass
 *
 * @author ahmedaljanabi
 */
class UserValidator extends \rueckgrat\security\ValidatorContainer {
    public function __construct(\app\mapper\User $user) {
    parent::__construct($user) ;
    
    $prename =  new validationRule('prename', ValidationRules::MIXED);
    $prename->setLengths(3, 50);
    $prename->setErrorMsgGlobal("Please entre a prename");
    
    $this->addRule($prename);
    
    $name =  new validationRule('name', ValidationRules::MIXED);
    $name->setLengths(3, 50);
    $name->setErrorMsgGlobal("Please entre a name");
    
    $this->addRule($name);
    
    $mail =  new validationRule('mail', ValidationRules::Email);
    $mail>setLengths(3, 50);
    $mail->setErrorMsgGlobal("Please entre a mail");
    
    $this->addRule($mail);
    
    
    }
    
    
    
}
